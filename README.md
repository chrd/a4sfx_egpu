# Dan A4-SFX eGPU adapter board #
![Rendering of the adapter board](a4sfx_thunder3.png)

3D printable adapter board for the Akitio Thunder3 PCB to fit into a mini-ITX case with a riser card, such as the Dan A4-SFX.
The model has been created using OpenSCAD and can be modified to fit other cases and eGPU boards.

Please note that the I/O shield does not have the cutout for the PSU connector.
