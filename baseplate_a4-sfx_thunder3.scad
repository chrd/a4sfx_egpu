itx_x = 170;
itx_y = 170;
surround = 6.35;
base_str = 2;
hole_h   = base_str * 2 + 1;
ioshield_len    = 158.57;
ioshield_height = 45;
projection = false;

module baseplate() {
    itx_hole_r = 2;
    offs_surr  = 10.16;
    offs_itxf  = 22.86;
    /* Mini-ITX baseplate */
    difference() {
        cube([itx_x,itx_y,base_str]);
        /* Mini-ITX mounting holes */
        /* hole F */
        translate([surround,offs_surr+offs_itxf,0])
            cylinder(r=itx_hole_r,h=hole_h,center=true,$fs = 0.01);
               
        /* hole C */
        translate([itx_x-surround,offs_surr,0])
            cylinder(r=itx_hole_r,h=hole_h,center=true,$fs = 0.01);
            
        /* hole H*/
        translate([itx_x-surround,itx_y-surround,0])
            cylinder(r=itx_hole_r,h=hole_h,center=true,$fs = 0.01);
            
        /* hole J*/
        translate([surround,itx_y-surround,0])
            cylinder(r=itx_hole_r,h=hole_h,center=true,$fs = 0.01);
    }
}

module ioshield(offset=0) {
     /* Mini-ITX I/O shield */   
    translate([offset*-3.75,-base_str*offset,0])
        cube([ioshield_len,base_str,ioshield_height]);
}

module t3_ioports() {
    x_offs    = 50; // offset from left hole C edge to DP-port
    port_offs = 2.2; // thickness of T3 PCB=2mm, 5mm spacer height, 1.5mm base height
    spacer    = 5 + port_offs; /* ToDo measure spacer length w/o screw */
    dp_len    = 19;
    dp_h      = 10;
    dp_offs   = ioshield_len-x_offs;
    /* Displayport ToDO measure actual size from T3 case */
    translate([dp_offs-dp_len/2-4,-hole_h,base_str+spacer])
        cube([dp_len,2*hole_h,dp_h]);
    
    /* TB3 Port 0 */
    tb3_w = 11;
    tb3_h = 5;
    offs_dp_tb30 = 11;
    tb30_baseoffs = dp_offs-dp_len-offs_dp_tb30;
    translate([tb30_baseoffs-tb3_w/2,-hole_h,base_str+spacer])
        cube([tb3_w,2*hole_h,tb3_h]);
    
    /* TB3 Port 1 */
    offs_tb30_tb31 = 11;
    tb31_baseoffs = tb30_baseoffs-tb3_w;
    translate([tb31_baseoffs-offs_tb30_tb31-tb3_w/2,-hole_h,base_str+spacer])
        cube([tb3_w,2*hole_h,tb3_h]);
    
}

module thunder3bp() {
    /* Thunder3 mounting holes */
    baseoffs      = itx_x-surround-18;
    t3_hole_r     = 1;
    offs_b        = 44;
    offs_t3a      = 30;
    offs_t3c      = 20;
    xoffs_t3e_t3f = 6;
    dist_itxc_t3c = 47;
    dist_t3d_t3c  = 83;
    dist_t3c_t3b  = 26;
    dist_t3e_t3f  = 33;
    dist_t3f_t3g  = 75; //offs_b+dist_t3e_t3f+dist_t3f_t3g+baseoffs
    
    /* T3 A */
    translate([baseoffs,offs_t3a+5,0])
            cylinder(r=t3_hole_r,h=hole_h,center=true,$fs = 0.01);
    
    /* T3 B */
    translate([baseoffs-dist_t3c_t3b-2,offs_b+3,0]) // re-do measurement, may be off 1 - 1.5 mm
            cylinder(r=t3_hole_r,h=hole_h,center=true,$fs = 0.01);
    
    /* T3 C */
    translate([baseoffs-dist_t3c_t3b-2,offs_t3c+3,0])
            cylinder(r=t3_hole_r,h=hole_h,center=true,$fs = 0.01);
    
    /* T3 D */
    translate([itx_x-surround-dist_itxc_t3c-dist_t3d_t3c+1,9+1,0])
            cylinder(r=t3_hole_r,h=hole_h,center=true,$fs = 0.01);
    
     /* T3 E */
    translate([itx_x-surround-dist_itxc_t3c-dist_t3d_t3c,offs_b+4,0])
            cylinder(r=t3_hole_r,h=hole_h,center=true,$fs = 0.01);
            
     /* T3 F */
    translate([itx_x-surround-dist_itxc_t3c-dist_t3d_t3c+xoffs_t3e_t3f+2,offs_b+dist_t3e_t3f+4,0])
            cylinder(r=t3_hole_r,h=hole_h,center=true,$fs = 0.01);
               
    /* T3 G */
    translate([itx_x-surround-dist_itxc_t3c-dist_t3d_t3c+xoffs_t3e_t3f+2,offs_b+dist_t3e_t3f+dist_t3f_t3g+3,0])
            cylinder(r=t3_hole_r,h=hole_h,center=true,$fs = 0.01);
            
    /* T3 H */
    translate([baseoffs-2,offs_b+dist_t3e_t3f+dist_t3f_t3g+5,0])
            cylinder(r=t3_hole_r,h=hole_h,center=true,$fs = 0.01);
}


module thermalstruts() {
    /* remove unnessary spaces from ITX baseplate */
    translate([50,55,-1])
        cube([90,120,hole_h]);
    translate([45,-2,-1])
        cube([65,65,hole_h]);
}

/* I/O shield only */
module ioshield_only() {
  /* move to origin */
    translate([ioshield_len, 0, 0])
    /* flip print side for prettier case rear */
    mirror([1, 0, 0])
         /* rotate flat for projection */
    rotate([90,0,0]) 
    difference() {
        ioshield();
        t3_ioports();
    }
}

module ioshield_upright() {
    difference() {
        ioshield();
        t3_ioports();
    }
}

/* board with ioshield, holes etc */
module miniitx_full() {
    difference() {
        baseplate();
        thunder3bp();
        thermalstruts();
    }
}

module mini_itx_upper() {
    difference() {
        miniitx_full();
        translate([0,0,-1])
         cube([170/2,170,2*hole_h]);
    }
}

module mini_itx_lower() {
    difference() {
        miniitx_full();
        translate([170/2+1,-2,-2])
         cube([170/2,174,2*hole_h]);
    }
}

//projection(cut=false)

//mini_itx_upper();
//mini_itx_lower();
ioshield_only();
